package com.afs.todolist.controller;

import com.afs.todolist.service.Dto.TodoRequest;
import com.afs.todolist.service.Dto.TodoResponse;
import com.afs.todolist.service.TodoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("todo")
public class TodoController {
    private TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getAllTodos() {
        return todoService.findAll();
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        return todoService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody TodoRequest todoRequest) {
        return todoService.addTodo(todoRequest);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable Long id,@RequestBody TodoRequest todoRequest) {
        return todoService.updateTodo(id,todoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Long id){
        todoService.deleteTodo(id);
    }
}
