package com.afs.todolist.service.Mapper;

import com.afs.todolist.entity.Todo;
import com.afs.todolist.service.Dto.TodoRequest;
import com.afs.todolist.service.Dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static Todo toEntity(TodoRequest todoRequest){
        Todo todo=new Todo();
        BeanUtils.copyProperties(todoRequest,todo);
        return todo;
    }
    public static TodoResponse toResponse(Todo todo){
        TodoResponse todoResponse=new TodoResponse();
        BeanUtils.copyProperties(todo,todoResponse);
        return todoResponse;
    }
}
