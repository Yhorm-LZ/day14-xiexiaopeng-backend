package com.afs.todolist.service;

import com.afs.todolist.entity.Todo;
import com.afs.todolist.exception.TodoNotFoundException;
import com.afs.todolist.repository.TodoJPARepository;
import com.afs.todolist.service.Dto.TodoRequest;
import com.afs.todolist.service.Dto.TodoResponse;
import com.afs.todolist.service.Mapper.TodoMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private TodoJPARepository todoJPARepository;

    public TodoService(TodoJPARepository todoJPARepository) {
        this.todoJPARepository = todoJPARepository;
    }

    public List<TodoResponse> findAll(){
        return todoJPARepository.findAll()
                .stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    public TodoResponse addTodo(TodoRequest todoRequest){
        return TodoMapper.toResponse(todoJPARepository.save(TodoMapper.toEntity(todoRequest)));
    }

    public TodoResponse updateTodo(Long id,TodoRequest todoRequest) {
        Todo updateTodo=todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if(todoRequest.getName() != null){
            updateTodo.setName(todoRequest.getName());
        }
        if(todoRequest.getDone() != null){
            updateTodo.setDone(todoRequest.getDone());
        }
        return TodoMapper.toResponse(todoJPARepository.save(updateTodo));
    }

    public void deleteTodo(Long id) {
        todoJPARepository.deleteById(id);
    }

    public TodoResponse findById(Long id) {
        return TodoMapper.toResponse(todoJPARepository.findById(id).orElseThrow(TodoNotFoundException::new));

    }
}
