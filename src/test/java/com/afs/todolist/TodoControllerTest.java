package com.afs.todolist;

import com.afs.todolist.entity.Todo;
import com.afs.todolist.repository.TodoJPARepository;
import com.afs.todolist.service.Dto.TodoRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoJPARepository todoJPARepository;

    @BeforeEach
    void setUp() {
        todoJPARepository.deleteAll();
    }


    @Test
    void should_create_todo() throws Exception {
        Todo todo = new Todo(1L, "task 1", false);
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setName(todo.getName());
        todoRequest.setDone(todo.getDone());
        ObjectMapper objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(todoRequest);

        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_update_todo() throws Exception {
        Todo todo = new Todo(1L, "task 1", false);
        Long todoId = todoJPARepository.save(todo).getId();
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setName("I' new todo1");
        todoRequest.setDone(!todo.getDone());
        ObjectMapper objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(todoRequest);

        mockMvc.perform(put("/todo/{id}", todoId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todoRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoRequest.getDone()));
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo(1L, "task 1", false);
        Long todoId = todoJPARepository.save(todo).getId();

        mockMvc.perform(delete("/todo/{id}", todoId))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Assertions.assertTrue(todoJPARepository.findById(todoId).isEmpty());
    }

    @Test
    void should_return_not_found_and_message_when_update_given_invalid_id() throws Exception {
        Todo todo = new Todo(1L, "task 1", false);
        Long todoId = todoJPARepository.save(todo).getId();
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setName("I' new todo1");
        todoRequest.setDone(!todo.getDone());
        ObjectMapper objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(todoRequest);

        mockMvc.perform(put("/todo/{id}", todoId - 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("todo id not found"));
    }

    @Test
    void should_return_not_found_and_message_when_find_todo_by_id_given_invalid_id() throws Exception {
        Todo todo = new Todo(1L, "task 1", false);
        Long todoId = todoJPARepository.save(todo).getId();

        mockMvc.perform(get("/todo/{id}", todoId - 1L))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("todo id not found"));
    }

    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo = new Todo(1L, "task 1", false);
        Long todoId = todoJPARepository.save(todo).getId();

        mockMvc.perform(get("/todo/{id}", todoId))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todoId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_return_todo_list() throws Exception {
        Todo todo = new Todo(1L, "task 1", false);
        Long todoId = todoJPARepository.save(todo).getId();

        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todoId))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(todo.getName()));
    }
}
